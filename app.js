const filmList = document.querySelector('#filmList')


function AffichageListe(doc){
    let li = document.createElement('li');
    let title = document.createElement('span');
    let genre = document.createElement('span');
    let year = document.createElement('span');
    let fav = document.createElement('span');

    li.setAttribute('id', doc.id);
    li.setAttribute('class', 'film');
    title.textContent = doc.data().title;
    genre.textContent = doc.data().genre;
    year.textContent = doc.data().year;
    if (doc.data().fav == true){
        fav.innerHTML = '<i class="fas fa-heart"></i>';
        fav.setAttribute('class', 'favT ' + JSON.stringify(doc.id));
        fav.setAttribute('onclick', "unfav(" + JSON.stringify(doc.id) + ")");
        
    }
    else {
        fav.innerHTML = '<i class="far fa-heart"></i>';
        fav.setAttribute('class', 'favF '+ JSON.stringify(doc.id));
        fav.setAttribute('onclick', 'fav(' + JSON.stringify(doc.id) + ')');

    }
    li.appendChild(title);
    li.appendChild(genre);
    li.appendChild(year);
    li.appendChild(fav);
    

    filmList.appendChild(li);
}






const film = db.collection('film').orderBy("year", "asc").get()
film.then((snap) =>{
    snap.docs.forEach(doc => {
        AffichageListe(doc)
    });
}
)

function unfav(id){
    li = document.getElementById(id);
    child = document.getElementsByClassName('favT "' + id + '"').item(0);
    let element = child.cloneNode();
    element.innerHTML = '<i class="far fa-heart"></i>';
    element.className = 'favF "'+ id + '"';
    element.setAttribute('onclick', 'fav("' + id + '")');
    li.replaceChild (element, child);

    var filmFav = db.collection("film").doc(id);

    return filmFav.update({
        fav: false
    })
    .then(() => {
        console.log("Document successfully updated!");
    })
    .catch((error) => {
        
        console.error("Error updating document: ", error);
    });
}

function fav(id){
    li = document.getElementById(id);
    child = document.getElementsByClassName('favF "' + id + '"').item(0);
    let element = child.cloneNode();
    element.innerHTML = '<i class="fas fa-heart"></i>';
    element.className = 'favT "'+ id + '"';
    element.setAttribute('onclick', 'unfav("' + id + '")');
    li.replaceChild (element, child);

    var filmFav = db.collection("film").doc(id);

    return filmFav.update({
        fav: true
    })
    .then(() => {
        console.log("Document successfully updated!");
    })
    .catch((error) => {
        
        console.error("Error updating document: ", error);
    });
}

function searchMovies(){
    let input = document.getElementById('searchBar').value
    input=input.toLowerCase();
    let x = document.getElementsByClassName('film');
      
    for (i = 0; i < x.length; i++) { 
        if (!x[i].innerText.toLowerCase().includes(input)) {
            x[i].style.display="none";
        }
        else {
            x[i].style.display="list-item";                 
        }
    }
}