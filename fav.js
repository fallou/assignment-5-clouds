const filmList = document.querySelector('#filmList')

function AffichageListe(doc){
    let li = document.createElement('li');
    let title = document.createElement('span');
    let genre = document.createElement('span');
    let year = document.createElement('span');
    let fav = document.createElement('span');

    li.setAttribute('id', doc.id);
    li.setAttribute('class', 'film');
    title.textContent = doc.data().title;
    genre.textContent = doc.data().genre;
    year.textContent = doc.data().year;

    fav.innerHTML = '<i class="fas fa-heart"></i>';
    fav.setAttribute('class', 'favT ' + JSON.stringify(doc.id));
    fav.setAttribute('onclick', 'unfav(' + JSON.stringify(doc.id) + ')');
    
    
    li.appendChild(title);
    li.appendChild(genre);
    li.appendChild(year);
    li.appendChild(fav);
    

    filmList.appendChild(li);
}


function AffichageVide(){
    content = document.querySelector('.content');
    document.getElementById("caption").style.display = "none";
    document.getElementById("searchBar").style.display = "none";


    let text = document.createElement('div');
    let button = document.createElement('button');
    text.textContent = "You did not add any movie to wishlist";
    button.innerText = "Movies List";

    button.setAttribute("onclick", "window.location='index.html'");
    content.appendChild(text);
    content.appendChild(button);
}


const film = db.collection('film').orderBy("title", "asc").get()
film.then((snap) =>{
    var empty = true;
    snap.docs.forEach(doc => {
        
        if (doc.data().fav == true){
            AffichageListe(doc)
            empty = false
        }
        
    });

    if(empty){
        AffichageVide();
    }
}
)

function unfav(id){
    let li = document.getElementById(id);
    filmList.removeChild (li);

    var filmFav = db.collection("film").doc(id);

    let lis = document.getElementsByClassName('film');
    if(lis.length == 0){
        AffichageVide();

    }

    return filmFav.update({
        fav: false
    })
    .then(() => {
        console.log("Document successfully updated!");
    })
    .catch((error) => {
        
        console.error("Error updating document: ", error);
    });

}

function searchMovies(){
    let input = document.getElementById('searchBar').value
    input=input.toLowerCase();
    let x = document.getElementsByClassName('film');
      
    for (i = 0; i < x.length; i++) { 
        if (!x[i].innerText.toLowerCase().includes(input)) {
            x[i].style.display="none";
        }
        else {
            x[i].style.display="list-item";                 
        }
    }
}
